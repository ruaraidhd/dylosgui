import datetime
import sys
import math

# Admiral, if we go "by the book", like
# Lieutenant Saavik, hours could seem like days.


def date_convert(minutes):
    """
    This is a convenience function. Input an int of minutes and receive
    a dictionary with days, hours and minutes totalling that number of minutes.
    """
    new_dict = {}
    new_dict["total minutes"] = minutes
    a = datetime.timedelta(minutes=new_dict["total minutes"])
    new_dict["days"] = a.days
    hours_remainder_in_seconds = \
        (a - datetime.timedelta(days=a.days)).total_seconds()
    hours_float = hours_remainder_in_seconds/3600.00
    new_dict["hours"] = math.floor(hours_float)
    mins_remainder_in_seconds = \
        hours_remainder_in_seconds - float(new_dict["hours"]*3600)
    new_dict["minutes"] = math.floor(mins_remainder_in_seconds/60)
    return new_dict


if __name__ == "__main__":
    mins = int(sys.argv[1])
    foo = date_convert(mins)
    print str(foo["days"]) + " days, " + \
        str(foo["hours"]) + " hours, " + \
        str(foo["minutes"]) + " minutes"
