AFRESH 0.4.0
-------------
Ruaraidh Dobson, University of Aberdeen
ruaraidh.dobson@abdn.ac.uk

What is it?
-----------

This software (AFRESH, formerly DylosGUI) provides an interface to Dylos(c)
air quality monitors, and implements conversion and display of data from 
cubic feet to PM2.5 ug/m3 equivalents as described in "Using a new, 
low-cost air quality sensor to quantify second-hand smoke (SHS) levels in 
homes", Sean Semple et al., Tobacco Control 2013

Components
----------

dylos2pm25.py		The main module, covering conversion of Dylos records to PM2.5
					equivalents and the generation of graphical reports of the
					data in HTML and PDF format.

dylos_serial.py 	Covers downloading air quality data from a Dylos device over a
					serial port link, and the creation of standard Dylos logs.

minutes2days.py		A helper utility for preparing a pleasantly formatted number
					of days, hours and minutes from a certain number of minutes

log_generator.py 	Creates fake logs for testing purposes

reports.py		Creates HTML and PDF reports using the Jinja2 templating system

dylosgui.py 		Graphical user interface to dylos2pm25, allowing the creation
					of reports simply and quickly.

tests/tests.py 		Unit testing suite

Dependencies
------------

wxPython 3.0+
numpy 1.8.1+
matplotlib 1.4.1+
pdfkit 0.4.1
wkhtmltopdf 0.2
appdirs 1.4.0

Documentation
-------------

Documentation for AFRESH can be found in help/help.html. Module documentation
is available as docstrings in the source (they could use work to conform to PEP
257).

License
-------

See LICENSE.txt for a full copy of the license. AFRESH is provided under the
GNU GPL v3.

Copyright (c) 2015 University of Aberdeen

AFRESH is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

AFRESH is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with AFRESH.  If not, see <http://www.gnu.org/licenses/>.