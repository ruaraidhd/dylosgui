#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import unittest
import os
import dylos2pm25.dylos2pm25 as d2


class check_for_dylos_log_test(unittest.TestCase):
    def test_blank(self):
        self.assertFalse(d2.check_for_dylos_log(open("tests/blank.txt", "rb")))

    def test_garbled(self):
        self.assertFalse(d2.check_for_dylos_log(open("tests/garbled.txt",
                                                     "rb")))


class find_time_breaks_tests(unittest.TestCase):
    def test_one_break(self):
        data = d2.read_file_to_dict(os.path.join("tests", "one_break_log.txt"))
        data = d2.calculate_values_dict(data)
        data = d2.scotify_dates(data)
        self.assertNotEqual(d2.find_time_breaks(data), [])


class parse_log_tests(unittest.TestCase):
    def test_good_log_from_disk(self):
        data = d2.read_file_to_dict(os.path.join("tests",
                                                 "real_log_no_breaks.txt"))
        self.assertTrue(data[0].has_key("Time"))
        self.assertTrue(data[0].has_key("Date"))
        self.assertTrue(data[0].has_key("Small particles per cubic foot"))
        self.assertTrue(data[0].has_key("Large particles per cubic foot"))

    def test_good_log_from_string(self):
        file_string = open(os.path.join("tests",
                                        "real_log_no_breaks.txt"), 'rb').read()
        data = d2.read_string_to_dict(file_string)
        self.assertTrue(data[0].has_key("Time"))
        self.assertTrue(data[0].has_key("Date"))
        self.assertTrue(data[0].has_key("Small particles per cubic foot"))
        self.assertTrue(data[0].has_key("Large particles per cubic foot"))

    def test_calculate_values(self):
        data = d2.read_file_to_dict(os.path.join("tests",
                                                 "real_log_no_breaks.txt"))
        self.assertEqual(d2.
                         get_log_type(open(os.path.join("tests",
                                                        "real_log_no_breaks.txt"),
                                           'r')), "Dylos Logger")
        dicts = d2.calculate_values_dict(data)
        self.assertIsInstance(dicts, list)
        self.assertIsNot(dicts, [])
        self.assertIsInstance(dicts[0], dict)
        max_pm25 = d2.get_maximum_PM25(dicts)
        self.assertIsInstance(max_pm25, float)
        # self.assertTrue()


class wkhtmltopdf_tests(unittest.TestCase):
    import wkhtmltopdf
    import pdfkit

if __name__ == "__main__":
    unittest.main()
