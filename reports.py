#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import sys
import os
import os.path
import pdfkit
import wkhtmltopdf
from jinja2 import Template
from dylos2pm25.dylos2pm25 import *


def make_report(dicts, filename="report.html",
                figure_filename="figure.png",
                first_day_fig_filename="firstday.png",
                large_particles_figure_filename="pass.png",
                template="default_template.html",
                for_graph=None,
                make_pdf=False,
                pdf_filename="report.pdf"):
    """
    Uses the jinja2 templating system to create reports. Provides a
    wide variety of data that can optionally be included in a template.
    """
    try:
        open_file = open(filename, 'wb')
    except IOError:
        print "IOError: cannot open " + filename
        sys.exit()

    report_template = Template(open(template, 'rb').read())
    time = get_time_running(dicts)
    name = filename.split(".")[0]  # Look, this is just silly.
    name = name.split(os.sep)[-1]  # This is just morally wrong.
    start = dicts[0]["Date"]
    end = dicts[-1]["Date"]
    total_time = str(time["days"]) + " days, " + \
        str(time["hours"]) + " hours and " + \
        str(time["minutes"]) + " minutes"
    max_PM25 = str(int(round(float(get_maximum_PM25(dicts)))))
    max_mult = str(int(math.floor(float(get_maximum_PM25(dicts))/25.00)))
    avg_PM25 = int(round(get_mean_PM25(dicts)))
    avg_over = str(int(math.floor(float(get_mean_PM25(dicts))/25.00)))
    time = get_time_running(dicts)

    # This is a percentage of the total time of the log
    time_over = round(get_time_over_25(dicts)*100/time["total minutes"])

    # Percentage of the total log over 246ug/m3, the average level in a smoky
    # pub
    time_over_bar = round(get_time_over_arb(dicts, 246) * 100 /
                          time["total minutes"])

    # Average hours per day that PM2.5 is above 25ug/m3
    hours_day_over = int(round((time_over/100.00)*24))

    if for_graph is None:
        PM25_fig = line_graph_PM25(dicts, filename=figure_filename)
    else:
        PM25_fig = line_graph_PM25(for_graph, filename=figure_filename)

    debug_log("PM25_fig: ", PM25_fig)
    # Fix to confirm to URI specs and to work with wkhtmltopdf
    PM25_fig = "file:///" + PM25_fig.replace("\\", "/")

    # Slices the list of dicts to get just the first day
    PM25_first_day_graph = line_graph_PM25(dicts[:1440],
                                           filename=first_day_fig_filename)
    PM25_first_day_graph = "file:///" + PM25_first_day_graph.replace("\\", "/")

    debug_log("Filename of first day graph: " + PM25_first_day_graph)
    large_particles_fig = \
        line_graph_large_particles(dicts,
                                   filename=large_particles_figure_filename)
    large_particles_fig = "file:///" + large_particles_fig.replace("\\", "/")

    # print report_template
    rendered = report_template.render(filename=name,
                                      start_date=start,
                                      end_date=end,
                                      time_running=total_time,
                                      max_value=max_PM25,
                                      max_multiple=max_mult,
                                      avg_value=avg_PM25,
                                      avg_mult=avg_over,
                                      graph=PM25_fig,
                                      first_day_graph=PM25_first_day_graph,
                                      time_over=time_over,
                                      time_over_246=time_over_bar,
                                      large_particles=large_particles_fig,
                                      hours_day_over=hours_day_over,)
    open_file.write(rendered)
    open_file.close()

    if make_pdf is True:
        debug_log("Making pdf from report file: " + filename +
                  " PDF filename: " + pdf_filename)
        opened_file = open(filename, 'rb')

        # Workaround for Windows XP, which has no where command
        wkhtmltopdf_location = "C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe"
        wkhtmltopdf_location_x86 = "C:\\Program Files (x86)\\wkhtmltopdf\\bin\\wkhtmltopdf.exe"
        config = None
        if os.path.exists(wkhtmltopdf_location):
            config = pdfkit.configuration(wkhtmltopdf=wkhtmltopdf_location)
        elif os.path.exists(wkhtmltopdf_location_x86):
            config = pdfkit.configuration(wkhtmltopdf=wkhtmltopdf_location_x86)
        pdfkit.from_file(opened_file, pdf_filename,
                         options={"Orientation": "Landscape"},
                         configuration=config)
    return filename
