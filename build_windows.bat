if exist dist\ del /s /q dist\
echo Bundling with pyinstaller
pyinstaller -y -D -n DylosGUI --distpath=dist -i icons\appbar.stock.ico dylosgui.py
echo Copying additional files
copy afresh.mplstyle dist\DylosGUI
xcopy /e /i icons dist\DylosGUI\icons
xcopy /e /i help dist\DylosGUI\help
copy /y default_template.html dist\DylosGUI
copy /y LICENSE.txt dist\DylosGUI
echo Building installer with Inno
"C:\Program Files (x86)\Inno Setup 5\ISCC.exe" /fDylosGUISetup .\create_installer.iss