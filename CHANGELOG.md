# Change Log
This is the change log for DylosGUI. It adheres to [Semantic Versioning](http://semver.org).

## [0.3.1][2015-11-16]
### Changed
- Expanded help file to take account of changes in v0.3.x
- DylosGUI now under GNU GPL v.3 (see LICENSE.txt)

### Fixed
- Expanded logging to catch errors in downloading

## [0.3.0][2015-07-21]
### Changed
- wx.html2.WebView replaces older HTML viewer to allow in-app viewing of reports using modern HTML
- Print functionality now integrated
- PDF viewing functionality now integrated
- Installer for wkhtmltopdf now integrated
- Adopted semantic versioning

### Fixed
- Code clean-up
- Data selector crashes on cancel/if no data selected no longer occur
- Records are now sorted by date internally, resolving bugs with dylos date-keeping (if records appeared to be in the future)