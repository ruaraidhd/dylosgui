#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import wx
import wx.html
import wx.richtext
import wx.html2
print "Loaded wx"
from dylos2pm25.dylos2pm25 import *
debug_log("dylos2pm25 loaded")
import dylos_serial as ds
debug_log("dylos_serial loaded")
import ConfigParser as cp
import reports
import sys
import os
import threading
import webbrowser
import appdirs

# This ensures that PyInstaller picks up these modules when creating an exe
import Tkinter
import FileDialog

VERSION = "v0.3.1"


class LogDisplay2(wx.richtext.RichTextCtrl):
    """
    Displays Dylos log text file. This is basically just a RichTextCtrl.
    """

    def __init__(self, parent):
        self.current_file = None
        wx.richtext.RichTextCtrl.__init__(
            self, parent,
            size=(300,
                  int(parent.
                  GetVirtualSizeTuple()[1]*0.95)
                  ),
            style=wx.TE_MULTILINE)
        font1 = wx.Font(10, wx.MODERN, wx.NORMAL, wx.NORMAL, False, u'Consolas')
        self.SetFont(font1)
        self.Show(True)

    def load(self, fileobject):
        self.SetValue(fileobject.read())

    def read(self):
        return self.GetValue()


class ReportDisplay(wx.html2.WebView):
    """
    Use a browser class renderer to show log reports,
    making the preview panel more useful.
    """
    def __init__(self, parent, id, size=(500, 500), size_tuple=None):
        if size_tuple is not None:
            internal_size = (size_tuple[0]/2, size_tuple[1])
            # print internal_size
        else:
            internal_size = size
        wx.html2.WebView.__init__(self, parent, id, internal_size)


class SettingsDialog (wx.Dialog):
    def __init__(self, parent):
        self.parent = parent
        wx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"Settings",
                           pos=wx.DefaultPosition,
                           size=wx.DefaultSize,
                           style=wx.DEFAULT_DIALOG_STYLE)
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)

        settings_grid = wx.GridSizer(6, 2, 0, 0)
        self.m_staticText1 = wx.StaticText(self, wx.ID_ANY,
                                           u"Save reports to this directory",
                                           wx.DefaultPosition,
                                           wx.DefaultSize,
                                           0)
        self.m_staticText1.Wrap(-1)
        settings_grid.Add(self.m_staticText1, 0, wx.ALL, 5)

        self.m_reports_directory = wx.DirPickerCtrl(self, wx.ID_ANY,
                                                    wx.EmptyString,
                                                    u"Select a folder",
                                                    wx.DefaultPosition,
                                                    wx.DefaultSize,
                                                    wx.DIRP_DEFAULT_STYLE)
        self.m_reports_directory.\
            SetForegroundColour(wx.SystemSettings.
                                GetColour(wx.SYS_COLOUR_WINDOWTEXT))

        settings_grid.Add(self.m_reports_directory, 0, wx.ALL, 5)

        self.m_staticText2 = wx.StaticText(self, wx.ID_ANY,
                                           u"Automatically open new reports \nin your browser?",
                                           wx.DefaultPosition,
                                           wx.DefaultSize,
                                           0)
        self.m_staticText2.Wrap(-1)
        settings_grid.Add(self.m_staticText2, 0, wx.ALL, 5)

        self.m_report_browser = wx.CheckBox(self, wx.ID_ANY,
                                            wx.EmptyString,
                                            wx.DefaultPosition,
                                            wx.DefaultSize,
                                            0)
        settings_grid.Add(self.m_report_browser, 0, wx.ALL, 5)

        self.m_staticText6 = wx.StaticText(self, wx.ID_ANY,
                                           u"Set threshold in mins to remove\n orphan records from start and end",
                                           wx.DefaultPosition,
                                           wx.DefaultSize,
                                           0)
        self.m_staticText6.Wrap(-1)
        settings_grid.Add(self.m_staticText6, 0, wx.ALL, 5)

        self.m_threshold = wx.SpinCtrl(self, wx.ID_ANY,
                                       wx.EmptyString,
                                       wx.DefaultPosition,
                                       wx.DefaultSize,
                                       wx.SP_ARROW_KEYS,
                                       0, 10, 0)
        self.m_threshold.SetRange(0, 60)
        settings_grid.Add(self.m_threshold, 0, wx.ALL, 5)

        self.m_staticText7 = wx.StaticText(self, wx.ID_ANY,
                                           u"Use this template to produce reports",
                                           wx.DefaultPosition,
                                           wx.DefaultSize,
                                           0)
        self.m_staticText7.Wrap(-1)
        settings_grid.Add(self.m_staticText7, 0, wx.ALL, 5)

        self.m_template_picker = wx.FilePickerCtrl(self,
                                                   wx.ID_ANY,
                                                   wx.EmptyString,
                                                   u"Select a file")
        settings_grid.Add(self.m_template_picker, 0, wx.ALL, 5)

        self.m_staticText8 = wx.StaticText(self, wx.ID_ANY,
                                           u"Make a PDF report as well as HTML",
                                           wx.DefaultPosition,
                                           wx.DefaultSize,
                                           0)
        self.m_staticText8.Wrap(-1)
        settings_grid.Add(self.m_staticText8, 0, wx.ALL, 5)

        self.m_make_pdf = wx.CheckBox(self, wx.ID_ANY, wx.EmptyString,
                                      wx.DefaultPosition, wx.DefaultSize, 0)
        settings_grid.Add(self.m_make_pdf, 0, wx.ALL, 5)

        self.m_save = wx.Button(self, wx.ID_ANY, u"Save",
                                wx.DefaultPosition,
                                wx.DefaultSize,
                                0)
        self.Bind(wx.EVT_BUTTON, self.save_settings, self.m_save)
        settings_grid.Add(self.m_save, 0, wx.ALL, 5)

        self.m_cancel = wx.Button(self, wx.ID_ANY, u"Cancel",
                                  wx.DefaultPosition,
                                  wx.DefaultSize,
                                  0)
        self.Bind(wx.EVT_BUTTON, self.cancel, self.m_cancel)
        settings_grid.Add(self.m_cancel, 0, wx.ALL, 5)

        self.SetSizer(settings_grid)
        self.Layout()
        settings_grid.Fit(self)

        self.Centre(wx.BOTH)
        self.set_boxes()
        self.Show(1)

    def __del__(self):
        # self.Destroy()
        pass

    def set_boxes(self):
        if self.parent.SETTINGS["reports"] is not None:
            self.m_reports_directory.SetPath(self.parent.SETTINGS["reports"])
        self.m_report_browser.SetValue(self.parent.SETTINGS["browser"])
        self.m_threshold.SetValue(self.parent.SETTINGS["threshold"])
        if self.parent.SETTINGS["template"] is not None:
            self.m_template_picker.SetPath(self.parent.SETTINGS["template"])
        self.m_make_pdf.SetValue(self.parent.SETTINGS["make_pdf"])

    def cancel(self, event):
        self.Destroy()

    def save_settings(self, event):
        if self.m_reports_directory.GetTextCtrlValue() != "":
            self.parent.SETTINGS["reports"] =\
                self.m_reports_directory.GetTextCtrlValue()
        if self.m_report_browser.GetValue():
            self.parent.SETTINGS["browser"] = True
        else:
            self.parent.SETTINGS["browser"] = False
        self.parent.SETTINGS["threshold"] = self.m_threshold.GetValue()
        if self.m_template_picker.GetTextCtrlValue() != "":
            self.parent.SETTINGS["template"] =\
                self.m_template_picker.GetTextCtrlValue()
        else:
            self.parent.SETTINGS["template"] = None
        debug_log(self.m_make_pdf.GetValue())
        if self.m_make_pdf.GetValue():
            self.parent.SETTINGS["make_pdf"] = True
        else:
            self.parent.SETTINGS["make_pdf"] = False
        self.parent.write_settings()
        self.Destroy()


class MainWindow(wx.Frame):
    """Main window"""

    SETTINGS = {
        "reports": None,
        "browser": True,
        "threshold": 10,
        "template": None,
        "make_pdf": True,
    }

    def __init__(self, parent, title, size=(800, 700)):

        debug_log("started running new dylosgui instance")

        splash = DylosSplashScreen()
        splash.Show()

        #IDs
        self.OPEN_TOOL_ID = self.NewControlId()
        self.DOWNLOAD_TOOL_ID = self.NewControlId()
        self.GENERATE_TOOL_ID = self.NewControlId()
        self.BROWSER_TOOL_ID = self.NewControlId()
        self.MAGIC_TOOL_ID = self.NewControlId()
        self.PRINT_TOOL_ID = self.NewControlId()
        self.PDF_TOOL_ID = self.NewControlId()
        debug_log("IDs loaded")

        #Read the settings to memory, and write them if they aren't there
        self.settings_file = os.path.join(appdirs.user_data_dir("DylosGUI"),
                                          "settings.ini")
        self.read_settings()

        # Initialise the Frame and Panels
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=title, size=size)
        debug_log("Frame exists")
        # self.Maximize()   # The default size set above works fine
                            # maximising seems a little OTT
        self.sb = self.CreateStatusBar()
        self.log = LogDisplay2(self)
        self.report = ReportDisplay.New(self,
                                        -1, size=self.GetVirtualSizeTuple())
        self.current_file = None
        debug_log("Panels exist")

        # Create the toolbar

        self.toolbar = self.CreateToolBar(style=wx.TB_FLAT)

        open_tool = self.toolbar.AddLabelTool(self.OPEN_TOOL_ID, "Open",
                                              wx.Bitmap(os.path.join(
                                                        "icons",
                                                        "appbar.folder.open.png")))
        self.Bind(wx.EVT_TOOL, self.open_file, open_tool)
        self.toolbar.SetToolShortHelp(self.OPEN_TOOL_ID, "Open a log file")

        download_tool = self.toolbar.\
            AddLabelTool(self.DOWNLOAD_TOOL_ID, "",
                         wx.Bitmap(os.path.join("icons",
                                   "appbar.cabinet.in.png")))
        self.Bind(wx.EVT_TOOL, self.download_log, download_tool)
        self.toolbar.SetToolShortHelp(self.DOWNLOAD_TOOL_ID,
                                      "Download a log from a connected Dylos")

        generate_tool = self.toolbar.\
            AddLabelTool(self.GENERATE_TOOL_ID, "",
                         wx.Bitmap(os.path.join("icons", "appbar.stock.png")))
        self.Bind(wx.EVT_TOOL, self.generate, generate_tool)
        self.toolbar.SetToolShortHelp(self.GENERATE_TOOL_ID,
                                      "Generate a report from the loaded log")

        magic_tool = self.toolbar.\
            AddLabelTool(self.MAGIC_TOOL_ID, "",
                         wx.Bitmap(os.path.join("icons",
                                                "appbar.camera.flash.png")),
                         wx.Bitmap(os.path.join("icons",
                                                "appbar.camera.flash.disabled.png")))
        self.Bind(wx.EVT_TOOL, self.magic, magic_tool)
        self.toolbar.\
            SetToolShortHelp(self.MAGIC_TOOL_ID,
                             "Download a Dylos log and display a report")

        ports = ds.get_ports()
        if ports == []:
            # FIXME - This is a slightly hacky way of displaying a message that
            # there's no Dylos attached. It's not ideal, and it doesn't work
            # perfectly on OS X, but it's better than nothing.
            # But it should really poll all the connected serial devices
            # and ask them if they're a Dylos.
            ports = ['NO DYLOS DETECTED!']

        if ports[0] == 'NO DYLOS DETECTED!':
            # FIXME - if there's another serial device (or something emulating
            # one) on the system, this won't grey out the magic tool
            self.toolbar.EnableTool(self.MAGIC_TOOL_ID, False)

        # self.toolbar.AddSeparator()
        self.port_combobox = wx.ComboBox(self.toolbar, -1,
                                         ports[0], choices=ports)
        self.toolbar.AddControl(self.port_combobox)

        browser_tool = self.toolbar.\
            AddLabelTool(self.BROWSER_TOOL_ID, "",
                         wx.Bitmap(os.path.join("icons", "appbar.browser.png")),
                         wx.Bitmap(os.path.join("icons",
                                                "appbar.browser.disabled.png")))
        self.Bind(wx.EVT_TOOL, self.browser_report, browser_tool)
        self.toolbar.EnableTool(self.BROWSER_TOOL_ID, False)
        self.toolbar.SetToolShortHelp(self.BROWSER_TOOL_ID,
                                      "Show the current report in your browser")

        print_tool = self.toolbar.\
            AddLabelTool(self.PRINT_TOOL_ID, "",
                         wx.Bitmap(os.path.join("icons",
                                                "appbar.printer.text.png")),
                         wx.Bitmap(os.path.join("icons",
                                                "appbar.printer.text.disabled.png")))
        self.Bind(wx.EVT_TOOL, self.print_report, print_tool)
        self.toolbar.EnableTool(self.PRINT_TOOL_ID, False)
        self.toolbar.SetToolShortHelp(self.PRINT_TOOL_ID,
                                      "Print the current report")

        pdf_view_tool = self.toolbar.\
            AddLabelTool(self.PDF_TOOL_ID, "",
                         wx.Bitmap(os.path.join("icons",
                                                "appbar.page.file.pdf.png")),
                         wx.Bitmap(os.path.join("icons",
                                                "appbar.page.file.pdf.disabled.png")))
        self.Bind(wx.EVT_TOOL, self.show_pdf, pdf_view_tool)
        self.toolbar.EnableTool(self.PDF_TOOL_ID, False)
        self.toolbar.SetToolShortHelp(self.PDF_TOOL_ID, "Show a PDF if created")

        self.toolbar.Realize()  # Show everything and arrange it properly
        debug_log("Toolbar exists")

        # The subject name box at the top left
        self.subject_name = wx.TextCtrl(self)
        self.subject_name.SetValue("Log_" +
                                   datetime.datetime.now().
                                   strftime("%y%m%d_%H%M"))

        # Arrange all the main elements - the log box, the
        # subject name control and the report control - in sizers
        # It works like this: box (the parent sizer) contains leftBox
        # and report_sizer. leftBox contains subject_name and
        # log, report_sizer contains report

        box = wx.BoxSizer(wx.HORIZONTAL)

        subjectBox = wx.BoxSizer(wx.HORIZONTAL)
        subjectPanel = wx.Panel(self)
        wx.StaticText(subjectPanel, wx.ID_ANY, " Subject name: ")
        subjectBox.Add(subjectPanel, 0, wx.EXPAND)
        subjectBox.Add(self.subject_name, 1, wx.EXPAND)

        leftBox = wx.BoxSizer(wx.VERTICAL)
        # leftBox.Add(self.subject_name, 1, wx.EXPAND)
        leftBox.Add(subjectBox, 0, wx.EXPAND)
        leftBox.Add(self.log, 1, wx.EXPAND)
        # leftBox.Fit(self) # I think this doesn't do anything...
        box.Add(leftBox, 1, wx.EXPAND)

        report_sizer = wx.BoxSizer(wx.VERTICAL)
        report_sizer.Add(self.report, 1, wx.EXPAND)
        box.Add(report_sizer, 2, wx.EXPAND)

        self.SetAutoLayout(True)
        self.SetSizer(box)
        self.Layout()

        #Create MenuBar

        menubar = wx.MenuBar()
        menu = wx.Menu()

        m_open = menu.Append(wx.ID_OPEN, "&Open\tCtrl-O", "Open a log file")
        self.Bind(wx.EVT_MENU, self.open_file, m_open)

        m_generate = menu.Append(wx.ID_ANY, "Generate report\tCtrl-G",
                                 "Generate a PM2.5 report")
        self.Bind(wx.EVT_MENU, self.generate, m_generate)

        m_browser = menu.Append(wx.ID_ANY, "Open report in browser",
                                "Open current report in your browser")
        self.Bind(wx.EVT_MENU, self.browser_report, m_browser)

        m_exit = menu.Append(wx.ID_EXIT, "Exit\tCtrl-Q",
                             "Close window and exit program.")
        self.Bind(wx.EVT_MENU, self.on_close, m_exit)

        menubar.Append(menu, "&File")

        edit_menu = wx.Menu()
        e_settings = edit_menu.Append(wx.ID_ANY, "&Settings\tCtrl-S",
                                      "Open settings window")
        self.Bind(wx.EVT_MENU, self.settings_dialog, e_settings)

        menubar.Append(edit_menu, "Edit")

        help_menu = wx.Menu()

        if DEBUG is True:
            h_debug = help_menu.Append(wx.ID_ANY, "Debug Log",
                                       "Show the Debug Log")
            self.Bind(wx.EVT_MENU, self.show_log, h_debug)

        h_about = help_menu.Append(wx.ID_ANY, "About", "About Dylos GUI")
        self.Bind(wx.EVT_MENU, self.about, h_about)
        h_help = help_menu.Append(wx.ID_ANY, "&Help\tCtrl-H",
                                  "Open help window")
        self.Bind(wx.EVT_MENU, self.help_window, h_help)

        menubar.Append(help_menu, "Help")

        self.SetMenuBar(menubar)
        self.SetSizer(box)
        self.SetAutoLayout(True)

        self.subject_name.SetFocus()
        self.subject_name.SetSelection(-1, -1)
        self.report_loaded = False
        self.report_file = None

        icon_png = wx.Bitmap(os.path.join("icons", "appbar.stock.png"))
        self.icon = wx.EmptyIcon()
        self.icon.CopyFromBitmap(icon_png)

        wx.Frame.SetIcon(self, self.icon)

        self.Show(True)

    def on_close(self, event):
        self.Destroy()

    def open_file(self, event):
        """Load a log from the filesystem"""
        dlg = wx.FileDialog(self, "Choose a file", os.getcwd(),
                            "", "*.*", wx.OPEN)
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            mypath = os.path.abspath(path)
            self.current_file = mypath
            self.log.load(open(mypath, 'r'))

    def help_window(self, event):
        x = os.path.join("help", "help.html")
        y = os.path.abspath(x)
        webbrowser.open(y)

    def settings_dialog(self, event):
        x = SettingsDialog(self)
        x.ShowModal()

    def read_settings(self):
        """
        Tries to read settings from settings.ini. If that can't be found,
        default settings are written to that file.
        """
        settings = cp.SafeConfigParser()
        try:
            open(self.settings_file, 'r').read()
        except:
            debug_log("settings.ini couldn't be read - is this first run?")
            self.write_settings()
        settings.readfp(open(self.settings_file, 'r'))
        reports = settings.get("config", "reports")
        if reports != "None":
            self.SETTINGS["reports"] = reports
        else:
            self.SETTINGS["reports"] = None

        browser = settings.get("config", "browser")
        if browser == "False":
            self.SETTINGS["browser"] = False
        else:
            self.SETTINGS["browser"] = True

        #Set threshold to get rid of orphan results from start and end of log.
        # Set to 0 to disable.
        threshold = settings.get("config", "threshold")
        self.SETTINGS["threshold"] = int(threshold)

        #Set template for report generation
        report_template = settings.get("config", "template")
        debug_log("Report template: " + report_template)

        if report_template != "None":
            self.SETTINGS["template"] = report_template
        else:
            self.SETTINGS["template"] = None

        #Make a PDF report?
        make_pdf = settings.get("config", "make_pdf")
        if make_pdf == "False":
            self.SETTINGS["make_pdf"] = False
        else:
            self.SETTINGS["make_pdf"] = True

    def write_settings(self):
        """
        Uses the ConfigParser library to write the settings
        currently in memory to a standard .ini file.
        """
        settings = cp.ConfigParser()
        settings.add_section("config")
        settings.set("config", "reports", str(self.SETTINGS["reports"]))
        settings.set("config", "browser", str(self.SETTINGS["browser"]))
        settings.set("config", "threshold", str(self.SETTINGS["threshold"]))
        settings.set("config", "template", str(self.SETTINGS["template"]))
        settings.set("config", "make_pdf", str(self.SETTINGS["make_pdf"]))
        if os.path.exists(appdirs.user_data_dir("DylosGUI")) is False:
            os.makedirs(appdirs.user_data_dir("DylosGUI"))
        settings.write(open(self.settings_file, 'w'))
        debug_log("Current settings: ", self.SETTINGS)

    def download_log(self, event):
        if self.port_combobox.GetValue() == "NO DYLOS DETECTED!":
            #FIXME - this is hacky. There should be an app-wide flag for
            # an available Dylos
            debug_log("Download attempted but no Dylos detected!")
            error_msg = "Dylos not detected.\n\nMake sure the device is turned \
on and properly plugged in, and that if you are using a USB-Serial \
converter cable you have installed any required drivers.\n\n\
Please close and reopen this programme to continue."
            dlg = wx.MessageDialog(self, error_msg, "Error",
                                   wx.OK | wx.ICON_WARNING)
            dlg.ShowModal()
            dlg.Destroy()
        else:
            self.sb.SetStatusText("Downloading data\
(this may take a minute...)")
            self.download_thread = DownloadThread(self)

    def browser_report(self, event):
        """
        Uses webbrowser library to open the default browser.
        Might not work on all systems.
        """
        if self.report_loaded:
            webbrowser.open('file://' + os.path.realpath(self.report_file))

    def generate(self, event):
        """
        Generates a standard report using the dylos2pm25 library
        and the template specified in settings (or a default one).
        Note that this function is only ever called by GenerateThread,
        to prevent the GUI hanging.
        """
        if self.current_file is None:
            error_msg = "No log loaded\n\n\
                Open a log or download data from a connected Dylos to continue."
            dlg = wx.MessageDialog(self, error_msg, "Error",
                                   wx.OK | wx.ICON_WARNING)
            dlg.ShowModal()
            dlg.Destroy()
        else:
            self.sb.SetStatusText("Generating report...")
            if check_for_dylos_log(open(self.current_file, 'r')):
                debug_log("current file is a dylos log")

                # This uses the file on disk, not the contents of the log panel.
                # foo = read_file_to_dict(self.current_file)
                # debug_log("read file to dict")
                # print self.current_file

                # This allows you to use the current contents of
                # the log panel instead of the file on disk.
                foo = read_string_to_dict(self.log.read())
                debug_log("read string to dict")

                bar = calculate_values_dict(foo)
                debug_log("calculated values")

                # Check for type of log. Dylos Logger has an "interesting" bug
                # where it converts some, but not all, dates into UK format.
                # By checking for that we can correct it if present.
                # This is maybe a hacky way to avoid the problem, but it's a
                # difficult problem to solve (is 3/4/15 a UK date or a US one?)

                if get_log_type(open(self.current_file, 'r')) == "Python Dylos":
                    debug_log("this is a Python Dylos log")
                    bar = scotify_all_dates(bar)
                    debug_log("all dates scotified")
                else:
                    debug_log("this is not a python dylos log")
                    bar = scotify_dates(bar)
                    debug_log("selected dates scotified")

                # Sort by datetime
                bar, needed_sorted = sort_dicts_by_date_time(bar)
                debug_log("sorted dicts by datetime")
                debug_log("needed sorted - " + str(needed_sorted))
                if needed_sorted is True:
                    wx.MessageDialog(self,
                                  "This log was out of order by date and has been sorted.",
                                  "Out of order log",
                                  wx.OK,
                                     ).ShowModal()

                bar = strip_orphan_values(bar, self.SETTINGS["threshold"])
                debug_log("orphan values stripped")

                data_times = get_data_times(bar)

                debug_log("Data times: ", data_times)
                debug_log("Length of data times list: ", len(data_times))

                if len(data_times) > 1:
                    choose_data_times = wx.\
                        MultiChoiceDialog(self,
"""This log contains several separate \
periods of data. \nThis may be \
because the Dylos has been \
switched off and then on again. \
\nTick the data you would like to \
include in the report.""",
                                          "Choose data", [str(x["start_data"]) +
                                          " to " + str(x["end_data"])
                                          for x in data_times])
                    choose_data_times.SetSelections(xrange(0, len(data_times)))
                    modal_data = choose_data_times.ShowModal()
                    if modal_data == 5101:
                        return
                    data_times_selected = choose_data_times.GetSelections()
                    # Without this, we get into trouble later trying to pop
                    # elements higher indices move down. This is a kludge,
                    # but it works.
                    data_times_selected.sort(reverse=True)
                    debug_log(data_times_selected)

                    debug_log("data_times pre-pop: ", data_times)
                    for i in data_times_selected:
                        try:
                            data_times.pop(i)
                        except IndexError as e:
                            debug_log("IndexError")
                            print sys.exc_clear()
                            print e
                            print i

                    debug_log("data_times: ", data_times)

                    for j in data_times:
                        debug_log("start and end: ",
                                  j["start_data"], j["end_data"])
                        debug_log("j: ", j)
                        bar = strip_between_datetimes(bar,
                                                      j["start_data"],
                                                      j["end_data"])
                    debug_log("Last records: ",
                              [datetime_from_record(x)
                              for x in bar[-60:]])

                # Here, we split the list that will be used to make the graphs
                # ("dicts_for_graph") from the list used for calculations
                # ("bar"). This makes sure that the averages don't include the
                # 0 records we're about to add (for the benefit of graphing).
                time_breaks = find_time_breaks(bar)
                if time_breaks is None:
                    debug_log("Data not selected")
                    return
                dicts_for_graph = \
                    generate_nil_records_for_time_breaks(bar,
                                                         time_breaks)
                debug_log("nil records generated")

                # FIXME - The app mixes up subject names and file paths in a
                # horrifying way at the moment. These two concepts must be
                # separated asap.
                file_base = self.subject_name.GetValue()

                if self.SETTINGS["reports"] is not None:
                    file_base = os.path.join(self.SETTINGS["reports"],
                                             file_base)
                else:
                    default_location = os.path.join(os.path.expanduser("~"),
                                                    "Documents", "Dylos Logs")
                    if os.path.isdir(default_location):
                        pass
                    else:
                        debug_log("Creating " + default_location)
                        os.makedirs(default_location)
                    file_base = os.path.join(default_location, file_base)

                # Checking for a non-default template file.
                if self.SETTINGS["template"] is not None:
                    report_file = reports.\
                        make_report(bar,
                                    filename=file_base+".html",
                                    figure_filename=file_base+".png",
                                    large_particles_figure_filename=file_base +
                                    "_large_particles.png",
                                    first_day_fig_filename=file_base +
                                    "_first_day.png",
                                    template=self.SETTINGS["template"],
                                    for_graph=dicts_for_graph,
                                    make_pdf=self.SETTINGS["make_pdf"],
                                    pdf_filename=file_base+".pdf")
                else:
                    report_file = reports.\
                        make_report(bar,
                                    filename=file_base+".html",
                                    figure_filename=file_base+".png",
                                    large_particles_figure_filename=file_base +
                                    "_large_particles.png",
                                    first_day_fig_filename=file_base +
                                    "_first_day.png",
                                    for_graph=dicts_for_graph,
                                    make_pdf=self.SETTINGS["make_pdf"],
                                    pdf_filename=file_base+".pdf")

                # Making a CSV is really for legacy support and error checking.
                # The software doesn't do anything with it.
                # In the future, it would be good for DylosGUI to move to
                # CSVs or a similar format natively for logs.
                make_csv(dicts_for_graph, filename=file_base+".csv")

                # Loads the new report file into the report
                # panel and sets the flag (and absolute path)
                # self.report.LoadFile(report_file)
                self.report.LoadURL(report_file)
                self.report_loaded = True
                self.report_file = report_file
            else:
                error_msg = "Error: " + self.current_file +\
                    " is not a properly formatted Dylos log"
                debug_log(error_msg)
                dlg = wx.MessageDialog(self, error_msg,
                                       "Error", wx.OK | wx.ICON_WARNING)
                dlg.ShowModal()
                dlg.Destroy()

            #Ready the Windows "show pdf" feature
            self.pdf_filename = file_base + ".pdf"
            if self.SETTINGS["make_pdf"] is True and\
                    sys.platform.startswith("win"):
                self.toolbar.EnableTool(self.PDF_TOOL_ID, True)

            # Set the "show in browser" button in the toolbar to active
            self.toolbar.EnableTool(self.BROWSER_TOOL_ID, True)

            # Set the "print" button in the toolbar to active
            self.toolbar.EnableTool(self.PRINT_TOOL_ID, True)

            # If the setting to show in the browser
            # automatically is true, open the browser window.
            if self.SETTINGS["browser"]:
                self.browser_report(None)
            self.sb.SetStatusText("")

    def print_report(self, event):
        self.report.Print()

    def show_pdf(self, event):
        os.startfile(self.pdf_filename)

    def show_log(self, event):
        os.startfile(os.path.join(appdirs.user_data_dir("DylosGUI"),
                                  "debug_log.txt"))

    def magic(self, event):
        """
        This function downloads a log and then produces a report,
        without interruption. Useful for checking a log quickly.
        """
        self.downloaded_flag = threading.Event()
        self.download_log(None)
        self.generate_threaded(None)
        if self.SETTINGS["browser"] is False:
            self.browser_report(None)

    def generate_threaded(self, event):
        self.generate_thread = GenerateThread(self)

    def about(self, event):
        message = wx.AboutDialogInfo()
        message.SetName("Dylos GUI")
        message.SetVersion(VERSION)
        message.SetCopyright("""
Copyright (c) 2014 British Lung Foundation, 2015 University of Aberdeen
            """)
        message.AddDeveloper("Ruaraidh Dobson <ruaraidh.dobson@abdn.ac.uk>")
        message.AddDeveloper("Built on work by Dr Sean Semple")
        icon_png = wx.Bitmap(os.path.join("icons", "appbar.stock.png"))
        icon = wx.EmptyIcon()
        icon.CopyFromBitmap(icon_png)
        message.SetIcon(icon)
        description = """
Dylos GUI is helper software designed to make using a Dylos DC1700 Air Quality
Monitor simpler when carrying out an air quality intervention. The software
provides a graphical user interface frontend to the Dylos2PM2.5 library, which
converts Dylos readings to PM2.5 equivalents (as described in Semple et al.
Tobacco Control Aug 2013 (2nd)), graphs those readings and produces a
suitably formatted report on second hand smoke in a home.
        """
        message.SetDescription(description)
        license = open("LICENSE.txt", 'r').read()
        message.SetLicence(license)
        # x = wx.MessageDialog(self, message, "About", wx.ICON_INFORMATION)
        wx.AboutBox(message)

class DylosSplashScreen(wx.SplashScreen):
    def __init__(self, parent=None):
        image = wx.Image(name=os.path.join("icons", 
                                           "splashscreen.png")).ConvertToBitmap()
        splash_style = wx.SPLASH_CENTER_ON_SCREEN | wx.SPLASH_TIMEOUT
        splash_duration = 1000
        wx.SplashScreen.__init__(self, image, splash_style, splash_duration,
                                 parent)
        # self.Bind(wx.EVT_CLOSE, self.OnExit)
        wx.Yield()


class DownloadThread(threading.Thread):
    """Stop the GUI from hanging during downloading"""
    def __init__(self, notify_window):
        debug_log("Starting DownloadThread")
        threading.Thread.__init__(self)
        self._notify_window = notify_window
        self.start()

    def run(self):
        filename = str(self._notify_window.subject_name.GetValue()).\
            replace(" ", "_") + '.txt'
        if self._notify_window.SETTINGS["reports"] is not None:
            filename = os.path.join(self._notify_window.SETTINGS["reports"],
                                    filename)
        else:
            filename = os.path.join(os.path.expanduser("~"),
                                    "Documents", "Dylos Logs", filename)
        debug_log("filename: " + filename)
        fn = ds.download_results(filename,
                                 self._notify_window.port_combobox.GetValue())
        if fn == "":
            return
        self._notify_window.current_file = fn
        self._notify_window.log.load(open(fn, 'r'))
        print self._notify_window.log
        self._notify_window.sb.SetStatusText("")

        # checks to see if this is the magic button
        try:
            self._notify_window.downloaded_flag.set()
        except AttributeError:
            pass
        return


class GenerateThread(threading.Thread):
    def __init__(self, notify_window):
        threading.Thread.__init__(self)
        self._notify_window = notify_window
        self.start()

    def run(self):
        self._notify_window.downloaded_flag.wait()
        self._notify_window.generate(None)
        return


if __name__ == "__main__":
    debug_log("Started DylosGUI " + VERSION)
    app = wx.App(False)
    debug_log("wx.App initialised")
    frame = MainWindow(None, "Dylos GUI", size=(800, 600))
    app.MainLoop()
