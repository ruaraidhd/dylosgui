#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

# Log Generator
# Makes fake dylos logs for testing

import random
import datetime
import sys
from dylos2pm25.dylos2pm25 import *


def get_header():
    version = "DC1700"
    now = str(datetime.date.today().month) + '/' + \
        str(datetime.date.today().day) + '/' + \
        str(datetime.date.today().year) + ' ' + \
        str(datetime.datetime.now().hour) + ':' + \
        str(datetime.datetime.now().minute)
    header = """-------------------------\r
Python Dylos v0.1\r
Unit: %s\r
Date/Time: %s\r
-------------------------\r
Particles per cubic foot\r
-------------------------\r
Date/Time, Small, Large\n\r
""" % (version, now)
    return header


def make_log_from_list(log):
    """
    Returns a dylos formatted log file from a list containing 3 item lists of
    dylos data
    """
    header = get_header()
    final_log = []
    # final_log += header
    for i in log:
        line = i[0] + ", " + i[1] + ", " + i[2] + '\n'
        # print line
        final_log.append(line)
        # final_log += line
    # print final_log
    return header + ''.join(final_log)


def generate_fake_log(length, filename=None, func="random walk"):
    """
    Generates a fake log (currently by a random walk method) of int length
    minutes, then  writes it to filename if applicable and returns it.
    """
    if length < 0:
        raise ValueError("length is a negative value (" + str(length) + ")")
    startdate = datetime.datetime.now()
    log = []
    last_small = 10000
    last_large = 1000
    for i in xrange(length):
        # print last_small, last_large
        now = startdate + datetime.timedelta(minutes=i)
        new_small = last_small + random.choice([-100, 100])
        if new_small < 0:
            new_small = last_small + 100
        new_large = last_large + random.choice([-100, 100])
        if new_large < 0:
            new_large = last_large + 100
        log.append([
            now.strftime("%m/%d/%y %H:%M"),
            str(new_small),
            str(new_large),
        ])
        last_small = new_small
        last_large = new_large
        # print log
    if filename:
        with open(filename, 'wb') as f:
            f.write(make_log_from_list(log))
    return make_log_from_list(log)

if __name__ == '__main__':
    try:
        print generate_fake_log(int(sys.argv[1]), str(sys.argv[2]))
    except IndexError:
        try:
            print generate_fake_log(int(sys.argv[1]))
        except IndexError:
            print "give a value in minutes and, optionally, a filename to run this command"
    